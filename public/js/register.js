function fillBirthdayField() {
	/*
	 * Fill the birthday dropdowns. TODO: Take care of leap year and display the
	 * days corresponding to the month
	 */
	var months = [ "January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December" ];
	var regMonth = $('#regMonth');
	for ( var i = 0; i < 12; i++) {
		regMonth.append($('<option></option>').val(i).html(months[i]));
	}
	var regDay = $('#regDay');
	for ( var i = 1; i <= 31; i++) {
		regDay.append($('<option></option>').val(i).html(i));
	}
	var regYear = $('#regYear');
	for ( var i = 1930; i <= 2013; i++) {
		regYear.append($('<option></option>').val(i).html(i));
	}
}

function readSelectedFile(outerEvt) {
	var f = outerEvt.target.files[0];

	if (!f.type.match('image.*')) {
		alert('Please choose an image file.');
		return;
	}

	var reader = new FileReader();

	reader.onload = (function(theFile) {
		return function(innerEvt) {
			$('#profileImage').attr('src', innerEvt.target.result);
		}
	})(f);

	reader.readAsDataURL(f);
}

$(document).ready(function() {
	fillBirthdayField();
	/*
	 * Redirect the click event on the image to the click event on the hidden
	 * input="file" element thereby opening the file dialog.
	 */
	$('#profileImage').click(function(e) {
		$('#profileImageValue').click();
	});

	$('#profileImageValue').change(function(e) {
		readSelectedFile(e);	
	});
});
