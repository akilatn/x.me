var request;		// Global AJAX request

function getCookie(name) {
    var parts = document.cookie.split(name + "=");
    if (parts.length == 2) {
        var x = parts.pop().split(";").shift();
        return decodeURIComponent(x);
    }
}

function loadContent(key) {
    $('#menu').collapse('hide');
    if (request)
        request.abort();    // Abort any ongoing request

    request = $.ajax({
        url: "/xme/menu",
        type: "POST",
        data: {"item" : key}
    });
    
    request.done(function(res, textStatus, jqXHR) {
        $('#content').html(res);
    });
    
    request.fail(function(jqXHR, textStatus, errorThrown) {
        console.error("The following error occurred: " + textStatus, errorThrown);
    });
    
    request.always(function() {
    });
}

$(document).ready(function() {
    $("#username").html(getCookie("userFullName"));
    $('#profileImage').attr('src', '/uploads/' + getCookie('username')+'.png');
    $("#userrooms").html(getCookie("userrooms"));
    $("#community").click(function(e) {
        loadContent("community");
    });
    $("#home").click(function(e) {
        document.location.href = "/xme"
    });
    $("#settings").click(function(e) {
        loadContent("settings");
    });
    $("#profile").click(function(e) {
        loadContent("profile");
    });
});


