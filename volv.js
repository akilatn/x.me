var express = require('express'), 
	passport = require('passport'), 
	http = require('http'), 
	path = require('path'), 
	LocalStrategy = require('passport-local').Strategy, 
	ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn, 
	fs = require('fs'), 
	level = require('level'),
        Q = require('q'),
        fs = require('fs'),
        nodemailer = require('nodemailer'),
        request = require('request');

/* ===== Helper functions ===== */
function getExtension(filename) {
    var ext = path.extname(filename || '').split('.');
    return ext[ext.length - 1];
}

/* ===== Configure SMTP & mail message ===== */

// Create a SMTP transport object
var transport = nodemailer.createTransport("SMTP", {
        //service: 'Gmail', // use well known service.
                            // If you are using @gmail.com address, then you don't
                            // even have to define the service name
        auth: {
            user: "mailvolv@gmail.com",
            pass: "volv.it2me"
        }
    });

console.log('SMTP Configured');



/* ===== Database related setup/functions ===== */

var db = level('./volv.db', {
	valueEncoding : 'json'
});

// seed user data
var seedUserData = [ {
	type : 'put',
	key : 'Users~akila',
	value : {
		name: 'akila',
		password : 'secret',
		email : 'akila@volve.org',
                CreatedOn : '7/1/2013',
		FirstName : 'Akila ',
                LastName : 'Natarajan',
                Rooms : [ { 
                            name : 'Xpressions',
                            role : 'participant'
                          },
                          {
                            name : 'Samskrita Bharati BK',
                            role : 'owner'
                          }
                        ],
                Messages : [ 
                             'message1', 'message2'
                           ] 
	}
},
{
	type : 'put',
	key : 'Rooms~Xpressions',
	value : {
		name: 'Xpressions',
                CreatedOn : '7/1/2013',
                Owner : 'akila',
                Users : [ { 
                            name : 'akila',
                            role : 'participant'
                          },
                          { name : 'sri',
                            role : 'owner'
                          }
                        ],
                Messages : [ 
                             'message1', 'message2'
                           ] 
	}
},
{
	type : 'put',
	key : 'Messages~message1',
	value : {
		message: 'Do we have class today?',
                PostedOn : '7/1/2013',
                PostedBy : 'Akila',
                ChildMessages : ['message2'],
                ParentMessaes : []
                },
},
{
	type : 'put',
	key : 'Messages~message2',
	value : {
		message: 'Yes, at 8 PM',
                PostedOn : '7/1/2013',
                PostedBy : 'Sri',
                ChildMessages : [],
                ParentMessaes : ['message1']
                },
},
];

// push the data to leveldb
db.batch(seedUserData, function(err) {
	if (err)
		return console.log('Level: batch processing error ', err);
});

// Path to the uploads directory. Also, create the directory, if it is not
// present.
var uploadsPath = __dirname + '/uploads/';
if (!fs.existsSync(uploadsPath)) {
        fs.mkdirSync(uploadsPath, 0777);
}

var app = express();
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
/*
 * Setup application middleware. The incoming request goes through each of these
 * middleware in the below order before finally reaching the application router
 * (app.router).
 */
// Express middlewares
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.cookieParser());
app.use(express.session({
	secret : 'keyboard cat'
}));
app.use(express.bodyParser());
app.use(express.methodOverride());

// Passport middlewares
app.use(passport.initialize());
app.use(passport.session());

// Finally, application routers.
app.use(app.router);
// Middleware to serve static content. We most likely don't need it as the
// static content is served by nginx.
app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

function readUserFromDB(username){
        var deferred = Q.defer();
        var user = {};
	db.get('Users~'+username, function(err,value) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
		user= value;
                deferred.resolve(user);
            }
        });
        return deferred.promise;
}

// Read all users from DB
// TODO: Should be filtered based on username
function readAllUsersFromDB(username){
        var deferred = Q.defer();
        var users = new Array();
        db.createReadStream({
            start : 'Users~a',
            end   : 'Users~z',
        })
        .on('data', function(data){
            users.push(data.value);
        })
        .on('error', function (err) {
            deferred.reject(new Error(err));
        })
        .on('close', function () {
            deferred.resolve(users);
        })
        .on('end', function () {
        });
       
        return deferred.promise;
}

function readAllMesgFromDB(){
        var deferred = Q.defer();
        var messages = new Array();
        db.createReadStream({
            start : 'Messages~',
        })
        .on('data', function(data){
            messages.push(data.value);
        })
        .on('error', function (err) {
            deferred.reject(new Error(err));
        })
        .on('close', function () {
            deferred.resolve(messages);
        })
        .on('end', function () {
        });

        return deferred.promise;

}

function readAllMesgForUser(username){
	var deferred = Q.defer();
	var messages = new Array();
	var mesgIDs = new Array();
        
	db.get('Users~'+username, function(err,data) {
	   if (err) {
		deferred.reject(new Error(err));
	   } else {
	        var user = data;

		var mesgStr = String(user.Messages);
		mesgIDs = mesgStr.split(',') ;

		var the_promises = [];
		mesgIDs.foreach(function (mesgID){
		     db.get('Messages~'+mesgIDs[i], function (err, value) {
                        if (err) {
                                deferred.reject(new Error(err));
                        } else {
				deferred.resolve(value.message);
			}
		     });
		     the_promises.push(deferred.promise);
		});

	  } 
	});
	return Q.all(the_promises);

}

function readMessagesfromDB(messageIDs){
    var the_promises = [];
    messageIDs.forEach(function (messageID){
        var deferred = Q.defer();
        db.get('Messages~'+messageID, function (err, value) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
                //just pushing the message part
		//console.log(JSON.stringify(value));
                deferred.resolve(value);
            }
        });
        the_promises.push(deferred.promise);
    });
    return Q.all(the_promises);
}

// Setup a local authentication strategy (passport-local provides this). This
// can be later enhanced to provide more
// authentication methods (Facebook, Google+, etc.)
passport.use(new LocalStrategy(
/*
 * Even though the default credential fields expected by LocalStrategy is
 * 'username' and 'password', we specify it explicitly to make it easier to
 * change it later.
 */
{
	usernameField : 'loginUsername',
	passwordField : 'loginPassword'
}, function(username, password, done) {
	/*
	 * Find the user by username. If there is no user with the provided
	 * credentials, set the user to `false' to indicate failure and set a flash
	 * mesage. Otherwise, return the authenticated `user'
	 */
        readUserFromDB(username)
        .then(function (user) {
	    if (user.password != password){
	        return done(null, false, {
	            message : 'Invalid password'
		});
             } else {
                return done (null,user);
             }
	})
        .done(); 
}));

passport.serializeUser(function(user, done) {
	done(null, user.name);
});

passport.deserializeUser(function(name, done) {
	readUserFromDB(name)
        .then(function (user) {
		done(null, user);
	})
        .done();
});

// TODO: Should move these application routers to ./routes
app.get('/xme', ensureLoggedIn('/xme/login'), function(req, res) {
	res.sendfile(__dirname + '/views/home.html');
});

app.get('/xme/login', function(req, res) {
	res.sendfile(__dirname + '/public/login.html');
});

app.get('/xme/logout', function(req, res) {
	req.logout();
	res.redirect('/xme/login');
});

app.get('/xme/getProfileData', function(req, res) {
        var username = req.cookies.username;
        var mesgIDs = new Array();

        readUserFromDB(username)
        .then(function (user) {
	    if (""+user !== "null" && ""+user !== "undefined"){	
                var mesgStr = String(user.Messages);
                mesgIDs = mesgStr.split(',');
                readMessagesfromDB(mesgIDs)
                 .then(function(messages) {
	            if (""+messages !== "null" && ""+messages !== "undefined"){	
		       user["usermesg"] = messages ; 
                       console.log(JSON.stringify(user));
                       res.write(JSON.stringify(user));
	               res.end();	
		    }			
                 });
	   }
	   else {
	        console.log("No user or too much delay");	
        	res.end();
		}
        });
});

app.get('/xme/getAllMesgs', function(req, res) {
        readAllMesgFromDB()
        .then(function (mesg) {
                //console.log(JSON.stringify(user));
                res.write(JSON.stringify(mesg));
                res.end();
        });
});


app.get('/xme/getCommunityData', function(req, res) {
        var username = req.cookies.username;
        readAllUsersFromDB(username)
        .then(function (users) {
                console.log(JSON.stringify("Users:"+users));
                res.write('{"Users":'+JSON.stringify(users) +'}');
                res.end();
        });
});

app.post('/xme/menu', ensureLoggedIn('/xme/login'), function(req, res) {
	item = req.body.item;
	res.sendfile(__dirname + '/public/' + item + '.html');
});

app.post('/xme/register-user', function(req, res, next) {
	// First, lets logout any stale user who might still be active in the session.
	req.logout();
	var username = req.body.regUsername;
	var password = req.body.regPassword;
	var emailID = req.body.regEmailID;
	var value = {
		password: password,
		email : emailID,
		name : username,
                FirstName: username,
                createdOn: new Date(),
                Rooms : new Array()
	};
	db.put('Users~' + username, value);
	/* Save the file in the ./uploads directory with the name set to the username */
	fs.readFile(req.files.profileImageValue.path, function(err, data) {
		var extn = getExtension(req.files.profileImageValue.name);
		var newPath = uploadsPath + username + ".png";
		fs.writeFile(newPath, data, function(err) {
			console.log(err);
		});
	});
	value.username = username;
	req.login(value, function(err) {
		if (err)
			return next(err);
		res.cookie('userFullName', value.FirstName);
		res.cookie('username', value.name);
	});
	res.redirect('room-info.html');
});

app.post('/xme/register-room-info', function(req, res, next) {
	var username = req.cookies.username;
        var roomname = req.body.room;
        var roomDetails = {
            name : roomname,
            //TODO: the roles should not be hard-coded but referred from constants. 
           role : 'Owner'
        }
	// First get the row corresponding to the user who is logged in.
	db.get('Users~' + username, function(error, data) {
		if (error) {
			console.log('Error retrieving ' + userid + '; ' + error);
			return next(err);
		}
		var roomList = new Array();
                roomList = data.Rooms; 
                roomList.push(roomDetails);
                data.Rooms = roomList;
		db.put('Users~' + username, data);
		console.log(data);
                //TODO: Not doing anything with the invitees for now. 
                //This has to be parsed and added to room table
        	data.emailInvitees = req.body.email;
	});
        //TODO: Setting this in a cookie for now.  Eventually, we should use session
        res.cookie('userrooms', JSON.stringify(roomDetails));
	res.redirect('/xme');
});

// Custom callback to do authentication and set a cookie so that home.html can
// retrieve the username.
// TODO: Should instead render the page dynamically (using handlebars).
app.post('/xme/login', function(req, res, next) {
	passport.authenticate('local', function(err, user, info) {
		if (err)
			return next(err);
		if (!user)
			return res.redirect('/xme/login');
		// User authenticated. Since we are using custom callback, user has to
		// be 'logged-in' explicitly.
		req.login(user, function(err) {
			if (err)
				return next(err);
                        var rooms = user.Rooms;
                        var roomNames = {};
                        for (var i=0; i < rooms.length; i++){
                        }
                        //TODO: Setting this in a cookie for now.  Eventually, we should use session
                        //Also validation to see if any of these is empty should be added
			res.cookie('userFullName', user.FirstName + user.LastName);
			res.cookie('username', user.name);
                        res.cookie('userrooms', JSON.stringify(rooms));
                        req.session.username = user.name;
			return res.redirect('/xme');
		});
	})(req, res, next);
});

app.post('/xme/register', function(req, res) {
	var emailID = req.body.EmailID;
	var counselorName = req.body.CounselorName;
	var grade = req.body.grade;
        fs.appendFileSync('EmailList.txt', emailID + '\t' + counselorName + '\t' + grade + '\n', encoding='utf8');

        //var form = r.form();
        var postData = {
            'action': 'subscribe',
            'blog_id': '55329508',
            'source': 'http://volvme.wordpress.com/',
            'sub-type': 'widget',
            'redirect_fragment': 'blog_subscription-4',
            '_wpnonce': '46382bdec5',
            'email': emailID };

        // Post to wordpress
        request.post( {
            uri: "https://subscribe.wordpress.com",
            headers:{'content-type': 'application/x-www-form-urlencoded'},
            body:require('querystring').stringify(postData)
            },function(err,res,body){
                  console.log(res.statusCode);
            });

        // Message object
        var message = {

            // sender info
            from: 'Volv.Me<mailvolv@gmail.com>',

            // Comma separated list of recipients
            to: emailID,

            // Subject of the message
            subject: 'Welcome to Volv', //

            headers: {
                'X-Laziness-level': 1000
            },

            html:'<p>Hi there, </p> <p>Welcome to <a href="www.volv.me">Volv</a>.  We are so excited to have you here.  You have been subscribed to our <a href="http://volvme.wordpress.com">blog</a> where we share college admission tips.   </p><p>If you have specific topics/questions you would like us to address, drop us a line. We look forward to hearing from you.</p><p>Meanwhile, we continue to furiously build the tools that you need to track your progress, and succeed with your college admission goals. </p><p>Sincerely,</p><p>The Volv.Me Team</p>'
            };
        transport.sendMail(message, function(error){
        if(error){
            console.log('Error occured');
            console.log(error.message);
            return;
        }

        // if you don't want to use this transport object anymore, uncomment following line
        //transport.close(); // close the connection pool
        });
        res.redirect('thankyou.html');
});

// Create and start our server
var port = app.get('port');
http.createServer(app).listen(port, function() {
	console.log('Express server listening on port ' + port);
});
